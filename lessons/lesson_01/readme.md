# Первое занятие

## 1. Возможности Python для математических расчётов
См. ноутбуки Jupyter в папке [jupyter_notebooks](jupyter_notebooks/)

## 2. Автоматизация отчёта в word
Скрипт находится в файле [docx_template.py](docx_template.py). Шаблон docx представлен в
файле [automated_report_template.docx](automated_report_template.docx), его можно
открыть, например, в Libre Office и отредактировать сохраняя jinja метки.

## 3. Экспорт в pdf
Для того, чтобы работал экспорт jupyter ноутбука (*.ipynb) в pdf необходимо установить
дополнительные зависимости:
```bash
sudo apt update
sudo apt install pandoc

# https://nbconvert.readthedocs.io/en/latest/install.html#installing-tex
sudo apt install texlive-xetex texlive-fonts-recommended texlive-plain-generic

# всё равно экспорт в VSCode русского текста не работает, можно пропустить
sudo apt install texlive-lang-cyrillic
```
